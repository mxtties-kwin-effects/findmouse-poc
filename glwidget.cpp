/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial Usage
** Licensees holding valid Qt Commercial licenses may use this file in
** accordance with the Qt Commercial License Agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Nokia.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtGui>
#include <QtOpenGL>
#include <QtDebug>

#include <math.h>

#include "glwidget.h"
#include "qtlogo.h"

#ifndef GL_MULTISAMPLE
#define GL_MULTISAMPLE  0x809D
#endif

GLWidget::GLWidget(QWidget *parent)
    : QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{
    logo = 0;
    t = 0;
    lastTime = 0;
    texture = -1;
    selectedEffect = 3;
    drawBackground = true;
    drawLogo = false;
    qtGreen = QColor::fromCmykF(0.40, 0.0, 1.0, 0.0);
    qtPurple = QColor::fromCmykF(0.39, 0.39, 0.0, 0.0);
}

GLWidget::~GLWidget()
{
}

QSize GLWidget::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize GLWidget::sizeHint() const
{
    return QSize(400, 400);
}

void GLWidget::setTime(int time)
{
    if (lastTime == time) return;
    lastTime = time;
    t = time / 100.f;
    updateGL();
}

void GLWidget::initializeGL()
{
    qglClearColor(qtPurple.dark());

    logo = new QtLogo(this, 64);
    logo->setColor(qtGreen.dark());

    //    glEnable(GL_CULL_FACE);
    glShadeModel(GL_SMOOTH);
    //    glEnable(GL_LIGHTING);
    //    glEnable(GL_LIGHT0);
    //    glEnable(GL_MULTISAMPLE);
    //    static GLfloat lightPosition[4] = { 0.5, 5.0, 7.0, 1.0 };
    //    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
    backgroundTexture = bindTexture(QPixmap("background.jpg"));
    qDebug() << "bg texture id = " << texture;

}

void GLWidget::effect1()
{
    int w = width();
    int h = height();
    glViewport(0,0,w,h);
    QString lightmap = "focus2.png";


    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0.0f,w,h,0.0f,-1.0f,1.0f);

    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA);
    glMatrixMode(GL_MODELVIEW);
    if (texture == -1) {
        texture = bindTexture(QPixmap(lightmap));
        qDebug() << "texture id = " << texture;
    }
    glEnable(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glEnable(GL_BLEND);
    glLoadIdentity();
    glBindTexture(GL_TEXTURE_2D, texture);
    float qx = qMax(mousePos.x(), w-mousePos.x());
    float qy = qMax(mousePos.y(), h-mousePos.y());
    float size = sqrt(pow(qx, 2) + pow(qy, 2));
//    qDebug() << "size = " << size;
    glTranslatef(mousePos.x(),mousePos.y(), 0);
    glBegin(GL_QUADS);
    glColor3f(0,0,0);
    float focusSize = 50.f;

    float s = (size / focusSize) - .5f;

    glTexCoord2f(-t*s,-t*s);
    glVertex2f(-size,-size);
    glTexCoord2f(1.f+t*s,-t*s);
    glVertex2f(size,-size);
    glTexCoord2f(1.f+t*s,1.f+t*s);
    glVertex2f(size,size);
    glTexCoord2f(-t*s,1.f+t*s);
    glVertex2f(-size,size);
    glEnd();
}

void GLWidget::effect2()
{
    int nbRays = 20;
    int focusSize = 50; //ray
    QColor col1 = Qt::yellow;
    QColor col2 = Qt::white;
    bool pulse = false;


//    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA);
    int w = width();
    int h = height();
    glViewport(0,0,w,h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0.0f,w,h,0.0f,-1000.0f,1000.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    float qx = qMax(mousePos.x(), w-mousePos.x());
    float qy = qMax(mousePos.y(), h-mousePos.y());
    float size = sqrt(pow(qx, 2) + pow(qy, 2));
//    qDebug() << "size = " << size;
    float sectionAngle = 360.f/nbRays;
    QVector2D v1(focusSize, 0);
    QVector2D v2(v1);
    QMatrix rotMat;
    rotMat = rotMat.rotate(sectionAngle);
    QPointF p =v2.toPointF();
    p = p*rotMat;
    v2 = QVector2D(p);
//    qDebug() << v1 << v2;
    float sectionDist = (v2 - v1).length();
//    qDebug() << "sectionDist = "<< sectionDist;
    v1 = QVector2D(focusSize + size, 0);
    v2 = v1;
    v2 = QVector2D(v2.toPointF() * rotMat);
    float farSectionDist = (v2 - v1).length();
//    qDebug() << "farsectiondist" << farSectionDist;
    glTranslatef(mousePos.x(),mousePos.y(), 0);
    glRotatef(t*sectionAngle*2, 0,0,1);
    for (int i=0;i<nbRays;i++) {
        float angle = sectionAngle * i;
//        if (angle > 270) {
//            glScissor(mousePos.x(),h-mousePos.y(),w-mousePos.x(),h/2);
//            glEnable(GL_SCISSOR_TEST);
//        } else glDisable(GL_SCISSOR_TEST);
        glPushMatrix();
//        qDebug() << "angle" << angle;
        glRotatef(angle, 0,0,1);
        glTranslatef(focusSize,0,0);
        glBegin(GL_QUADS);
        QColor col = (i % 2 == 0 ? col1 : col2);

        glColor4f(col.redF(), col.greenF(), col.blueF(), pulse ? t : 0);
        glVertex2f(0,0);
        glColor4f(col.redF(), col.greenF(), col.blueF(), pulse ? t : 0);
        glVertex2f(size,0);
        glColor4f(col.redF(), col.greenF(), col.blueF(), 1);
        glVertex2f(size,farSectionDist);
        glColor4f(col.redF(), col.greenF(), col.blueF(), 1);
        glVertex2f(0,sectionDist);
        glEnd();
        glPopMatrix();
    }
}

void GLWidget::effect3()
{
    int nbRays = 20;
    int nbRayParts = 8;
    int focusSize = 50; //ray
    QColor col = Qt::white;
    QString tex = "arrow.png";


    if (texture == -1) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        texture = bindTexture(QPixmap(tex));
        qDebug() << "texture id = " << texture;
    }
    glBindTexture(GL_TEXTURE_2D, texture);

//    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    int w = width();
    int h = height();
    glViewport(0,0,w,h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0.0f,w,h,0.0f,-1000.0f,1000.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    float qx = qMax(mousePos.x(), w-mousePos.x());
    float qy = qMax(mousePos.y(), h-mousePos.y());
    float size = sqrt(pow(qx, 2) + pow(qy, 2));
//    qDebug() << "size = " << size;

    float sectionAngle = 360.f/nbRays;
    float rayPartSize = size / nbRayParts;

    QMatrix rotMat;
    rotMat = rotMat.rotate(sectionAngle / 2.f);

    QVector2D vFocus1(focusSize, 0);
    QPointF pFocus(focusSize, 0);
    pFocus = pFocus*rotMat;
    QVector2D vFocus2(pFocus);

    QVector2D vRayPart1(rayPartSize, 0);
    QPointF pRay(rayPartSize, 0);
    pRay = pRay*rotMat;
    QVector2D vRayPart2(pRay);

//    qDebug() << "vfocus" << vFocus1 << vFocus2;
//    qDebug() << "vraypart" << vRayPart1 << vRayPart2;
//////////////////
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
//glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    qglColor(col);
    glTranslatef(mousePos.x(),mousePos.y(), 0);
    for (int i=0;i<1;i++) {
        for (int j=0;j<nbRayParts;j++) {
            glBegin(GL_QUADS);
            glTexCoord2f(t,0.f);
            glVertex2f((vFocus1 + j * vRayPart1).length(),0);
            glTexCoord2f(1.f+t,0.f);
            glVertex2f((vFocus1 + (j+1) * vRayPart1).length(),0);
            glTexCoord2f(1.f+t,.5f);
            QVector2D v = vFocus2 + (j+1) * vRayPart2;
            glVertex2f(v.x(), v.y());
            glTexCoord2f(t,.5f);
            v = v - vRayPart2;
            glVertex2f(v.x(), v.y());
            glEnd();
        }
        glRotatef(sectionAngle/2.f, 0,0,1);
        for (int j=0;j<nbRayParts;j++) {
            glBegin(GL_QUADS);
            glTexCoord2f(t,0.5f);
            glVertex2f((vFocus1 + j * vRayPart1).length(),0);
            glTexCoord2f(1.f+t,0.5f);
            glVertex2f((vFocus1 + (j+1) * vRayPart1).length(),0);
            glTexCoord2f(1.f+t,1.f);
            QVector2D v = vFocus2 + (j+1) * vRayPart2;
            glVertex2f(v.x(), v.y());
            glTexCoord2f(t,1.f);
            v = v - vRayPart2;
            glVertex2f(v.x(), v.y());
            glEnd();
        }
        glRotatef(sectionAngle/2.f, 0,0,1);
    }
//    glRotatef(25, 0,0,1);
//    float fan = 20.f;
//    float tmpsize = 50.f;
//                glBegin(GL_QUADS);
//                glTexCoord2f(0,0.f);
//                glVertex2f(0,0);
//                glTexCoord2f(1.f,0.f);
//                glVertex2f(tmpsize,-t * fan);
//                glTexCoord2f(1.f,1.f);
//                glVertex2f(tmpsize,tmpsize+t * fan);
//                glTexCoord2f(0,1.f);
//                glVertex2f(0,tmpsize);
//                glEnd();
}

void GLWidget::paintGL()
{
    static int xRot = 0;
    int w = width();
    int h = height();
//    qDebug() << "w" << w << "h" << h;
    int side = qMin(w, h);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable(GL_DEPTH_TEST);
    if (drawLogo) {
        glViewport((w - side) / 2, (h - side) / 2, side, side);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(-0.5, +0.5, -0.5, +0.5, 4.0, 15.0);
        glMatrixMode(GL_MODELVIEW);

        glLoadIdentity();
        glTranslatef(0.0, 0.0, -10.0);
        glRotatef(++xRot, 1.0, 0.0, 0.0);
        glRotatef(xRot / 16.0, 0.0, 1.0, 0.0);
        glRotatef(xRot / 16.0, 0.0, 0.0, 1.0);
        glColor3f(0,1,0);
        glDisable(GL_TEXTURE_2D);
        glDisable(GL_BLEND);

        logo->draw();
        glLoadIdentity();
    }

    if (drawBackground) {
        glViewport(0,0,w,h);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0.0f,w,h,0.0f,-1000.0f,1000.0f);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        qglColor(Qt::white);
        glDisable(GL_BLEND);
        drawTexture(QRectF(0,0,w,h), backgroundTexture);
        glLoadIdentity();
    }

    switch (selectedEffect) {
    case 1: effect1();break;
    case 2: effect2();break;
    case 3: effect3();break;
    default: effect1();
    }
}

void GLWidget::resizeGL(int width, int height)
{
    int side = qMin(width, height);
    glViewport((width - side) / 2, (height - side) / 2, side, side);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
#ifdef QT_OPENGL_ES_1
    glOrthof(-0.5, +0.5, -0.5, +0.5, 4.0, 15.0);
#else
    glOrtho(-0.5, +0.5, -0.5, +0.5, 4.0, 15.0);
#endif
    glMatrixMode(GL_MODELVIEW);
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    mousePos = event->pos();
    qDebug() << "mouse pos" << mousePos;
    updateGL();
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
//    int dx = event->x() - mousePos.x();
//    int dy = event->y() - mousePos.y();

    if (event->buttons() & Qt::LeftButton) {
        mousePos = event->pos();
        updateGL();
    }
//        setXRotation(t + 8 * dy);
//    } else if (event->buttons() & Qt::RightButton) {
//        setXRotation(t + 8 * dy);
//    }
    mousePos = event->pos();
}
